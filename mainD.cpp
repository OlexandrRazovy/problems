#include <iostream>
#include <cmath>

using namespace std;

int main(int argc, char** argv) {
	int num = 0;
	cin >> num;
	double a = 0;
	double temp = (double)modf(num/100,&a);
	double b = 0;
	double c = modf(temp*10,&b)*10;
	cout << a << " " << b << " " << c << " " << temp;
	return 0;
}
