#include <iostream>
#include <cmath>

using namespace std;

int main(int argc, char** argv) {
	float x1 = 0;
	float y1 = 0;
	float x2 = 0;
	float y2 = 0;
	cin >> x1;
	cin >> y1;
	cin >> x2;
	cin >> y2;
	printf("%.3f",sqrt(pow(x2-x1,2)+pow(y2-y1,2)));
	return 0;
}
