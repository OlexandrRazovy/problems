#include <iostream>
#include <cmath>

using namespace std;

int main(int argc, char** argv) {
	float a = 1;
	int i = 0;
	int b = 0;
	while(true) {
		cin >> b;
		if(b == 0) break;
		a *= b;
		i++;
	}
	printf("%.3f",pow(a,1.0/i));
	return 0;
}
